# file: $GOPATH/cm-hundred-sequence/features/list.feature


Feature: Testing cdmon numeric list
  Users should be able to see numbers from 1 to 100 except those what are multiple of three or five.
  In case of number is multiple of three, it has to be shown 'cd'.
  In case of number is multiple of five, it has to be shown 'mon'.
  In case of number both multiple of three and five, it has to be shown 'cdmon'.

  Scenario: Multiples of three are replaced for 'cd'
    When user receives a list of items
    Then user should have 'cd' instead of numbers multiple of three and not of five
      | 1 |
      | 2 |
      | cd |
      | 4 |

  Scenario: Multiples of five are replaced for 'mon'
    When user receives a list of items
    Then user should have 'cd' instead of numbers multiple of five and not of three
      | 4 |
      | mon |
      | cd |
      | 7 |
      | 8 |

  Scenario: Multiples of three and five are replaced for 'cdmon'
    When user receives a list of items
    Then user should have 'cdmon' instead of numbers both multiple of five and three
      | 14 |
      | cdmon |
      | 16 |
      | 17 |