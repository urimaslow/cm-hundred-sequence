## Deployment

In order to run this application it is simply necessary to build main.go and run the executable file.  

> go build main.go 


---

## Test

It has been included BDD tests developed with Gherkin. You can find these in features folder. This command permits to execute them:
> go get github.com/DATA-DOG/godog/cmd/godog