package main

import (
	"github.com/DATA-DOG/godog"
	"github.com/DATA-DOG/godog/gherkin"
)


func userReceivesAListOfItems() error {
	m := createMap()
	if len(m) == 100 {
		return nil
	}
	return godog.ErrPending
}

func userShouldHaveCdInsteadOfNumbersMultipleOfThreeAndNotOfFive(control *gherkin.DataTable) error {
	m := createNumericTestMap(1, 4)
	if itemsAreTheSame(m, control){
		return nil
	}
	return godog.ErrPending
}

func userShouldHaveCdInsteadOfNumbersMultipleOfFiveAndNotOfThree(control *gherkin.DataTable) error {
	m := createNumericTestMap(4, 8)
	if itemsAreTheSame(m, control){
		return nil
	}
	return godog.ErrPending
}

func userShouldHaveCdmonInsteadOfNumbersBothMultipleOfFiveAndThree(control *gherkin.DataTable) error {
	m := createNumericTestMap(14, 17)
	if itemsAreTheSame(m, control){
		return nil
	}
	return godog.ErrPending
}

func createNumericTestMap(from int, to int)(m map[int]int) {
	m = make(map[int]int)
	for i := from; i <= to; i++ {
		m[i] = i
	}
	return
}

func itemsAreTheSame (m map[int]int, table *gherkin.DataTable)(bool) {
	i := 0
	for _, v := range m {
		if convertToString(m[v]) != table.Rows[i].Cells[0].Value {
			return false
		}
		i++
	}
	return true
}

func FeatureContext(s *godog.Suite) {
	s.Step(`^user receives a list of items$`, userReceivesAListOfItems)
	s.Step(`^user should have \'cd\' instead of numbers multiple of three and not of five$`, userShouldHaveCdInsteadOfNumbersMultipleOfThreeAndNotOfFive)
	s.Step(`^user should have \'cd\' instead of numbers multiple of five and not of three$`, userShouldHaveCdInsteadOfNumbersMultipleOfFiveAndNotOfThree)
	s.Step(`^user should have \'cdmon\' instead of numbers both multiple of five and three$`, userShouldHaveCdmonInsteadOfNumbersBothMultipleOfFiveAndThree)
}