package main

import (
	"fmt"
	"strconv"
)

const (
	size = 100
	cd   = "cd"
	mon  = "mon"
	void = ""
)

func main() {
	m := createMap()
	printValues(m)
}

func createMap() (m map[int]string) {
	m = make(map[int]string)
	for i := 1; i <= size; i++ {
		m[i] = convertToString(i)
	}
	return
}

func convertToString(i int) (s string) {
	s = ""
	if i%3 == 0 {
		s = cd
	}
	if i%5 == 0 {
		s = s + mon
	}
	if s == void {
		s = strconv.Itoa(i)
	}
	return
}

func printValues(m map[int]string) {
	for i := 1; i <= size; i++ {
		fmt.Println(m[i])
	}
}

